# Incanter's Flow Tracker #

Provides a global function that returns information about the "Incanter's Flow" spell, including its cycle progression.

Usage: "UnitIncantersFlow()"