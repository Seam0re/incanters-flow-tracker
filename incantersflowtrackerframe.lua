---------------------------------------------------------------------------
-- Provides a frame displaying the data returned by the tracker function --
---------------------------------------------------------------------------

-- Only run for Mages.
local class = strlower(select(2, UnitClass("player")))
if class and class ~= "mage" then return end

--- Global properties.
------------------------
local GetTime, select, math = GetTime, select, math

--- Local properties.
-----------------------
local trackerFrame = CreateFrame("Frame", "IncantersFlowTrackingFrame", UIParent)
local gradientScale = 0.05
local mtSVars = {
    show               = true,
    point              = { "CENTER", 0, 0, },
    trackerHeight      = 150,
    trackerWidth       = 25,
    trackerOrientation = "VERTICAL",
};

-- Sets the frame dimensions.
local setDimensions = function()
    local padX = (IFTSettings.trackerOrientation == "VERTICAL" and 2 or 4)
    local padY = (IFTSettings.trackerOrientation == "VERTICAL" and 4 or 2)

    trackerFrame.fillTexture:SetPoint         ("BOTTOMLEFT", padX, padY)
    trackerFrame.fillTexture.gradient:SetPoint("BOTTOMLEFT", padX, padY)

    trackerFrame:SetWidth (IFTSettings.trackerWidth  + 4)
    trackerFrame:SetHeight(IFTSettings.trackerHeight + 4)

    trackerFrame.fillTexture:SetWidth (IFTSettings.trackerWidth)
    trackerFrame.fillTexture:SetHeight(IFTSettings.trackerHeight)

    trackerFrame.fillTexture.gradient:SetWidth (IFTSettings.trackerWidth)
    trackerFrame.fillTexture.gradient:SetHeight(IFTSettings.trackerHeight)

    trackerFrame.fillTexture.gradient:SetGradientAlpha(IFTSettings.trackerOrientation, 0.824, 0.157, 0.196, 1,0.824, 0.157, 0.196, 0)
end

-- Reesource monitor function.
local onUpdate = function(display)
    local height         = IFTSettings.trackerOrientation == "VERTICAL" and IFTSettings.trackerHeight or IFTSettings.trackerWidth
    local cur, max, prog = select(3, UnitIncantersFlow())
    local expires        = (prog and (GetTime() + (max * prog) + 0.05) or 1)
    local duration       = (max or 1)
    local rem            = (expires - GetTime()) / duration

    cur = (cur and 1 or 0)
    rem = rem > 1 and 1 or rem

    if cur == 1 and (rem >= 0) and (rem <= 1) then
        local textCoordTop = (rem - gradientScale) - 4
        local newHeight    = (rem * height) - 4

        -- Check fill texture boundaries.
        textCoordTop = textCoordTop < 0 and 0 or textCoordTop
        rem          = rem          < 0 and 0 or rem
        newHeight    = newHeight    < 1 and 1 or newHeight

        -- Update the fill texture.
        if IFTSettings.trackerOrientation == "VERTICAL" then
            display.fillTexture:SetHeight  (newHeight)
            display.fillTexture:SetTexCoord(0, 1, math.abs(textCoordTop - 1), 1)
        else
            display.fillTexture:SetWidth   (newHeight)
            display.fillTexture:SetTexCoord(math.abs(textCoordTop - 1), 1, 1, 1)
        end

        local gradientTextCoordBottom = 1 - (rem)
        local gradientTextCoordTop    = rem
        local gradientHeight          = height * gradientScale

        -- Check gradient texture boundaries.
        gradientTextCoordBottom = gradientTextCoordBottom < gradientScale       and gradientScale       or gradientTextCoordBottom
        gradientTextCoordTop    = gradientTextCoordTop    > (1 - gradientScale) and (1 - gradientScale) or gradientTextCoordTop
        gradientTextCoordBottom = gradientTextCoordBottom > 1                   and 1                   or gradientTextCoordBottom

        local newY = (height * (1 - gradientTextCoordBottom))

        -- Update the gradient texture.
        if IFTSettings.trackerOrientation == "VERTICAL" then
            display.fillTexture.gradient:SetHeight  (gradientHeight)
            display.fillTexture.gradient:SetTexCoord(0, 1, math.abs(gradientTextCoordTop - 1), math.abs(gradientTextCoordBottom))
            display.fillTexture.gradient:SetPoint   ("BOTTOMLEFT", 2, newY)
        else
            display.fillTexture.gradient:SetWidth  (gradientHeight)
            display.fillTexture.gradient:SetTexCoord(math.abs(gradientTextCoordBottom), math.abs(gradientTextCoordTop - 1), 1, 0)
            display.fillTexture.gradient:SetPoint   ("BOTTOMLEFT", newY, 2)
        end

        -- Show the frame.
        display:SetAlpha(1)
        display:EnableMouse(true)
    else
        -- Hide the frame.
        display:SetAlpha(0)
        display:EnableMouse(false)
    end
end

-- Tracking Frame.
local createTrackerFrame = function()
    -- Load  Saved Variables.
    IFTSettings = IFTSettings or {}
    setmetatable(IFTSettings, { __index = mtSVars });

    -- Configure the tracker frame.
    trackerFrame:SetPoint (unpack(IFTSettings.point))
    trackerFrame:SetBackdrop{
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = {
            left = 0,
            right = 0,
            top = 0,
            bottom = 0
        }
    }
    trackerFrame:SetBackdropColor(1,1,1,0)

    -- Add fill texture and gradient.
    trackerFrame.fillTexture = trackerFrame:CreateTexture(nil, "BACKGROUND")
    trackerFrame.fillTexture:SetColorTexture(1,1,1,1)
    trackerFrame.fillTexture:SetVertexColor (0.824, 0.157, 0.196, 1)

    trackerFrame.fillTexture.gradient = trackerFrame:CreateTexture(nil, "BACKGROUND")
    trackerFrame.fillTexture.gradient:SetColorTexture(1,1,1,1)
    setDimensions()

    -- Add drag positioning.
    trackerFrame:SetClampedToScreen(true)
    trackerFrame:SetMovable(true)
    trackerFrame:EnableMouse(true)
    trackerFrame:RegisterForDrag("LeftButton")
    trackerFrame:SetScript("OnDragStart", function(self)
        if IsShiftKeyDown() then
            self:StartMoving()
        end
    end)
    trackerFrame:SetScript("OnDragStop", function(self)
        self:StopMovingOrSizing()
        IFTSettings.point = {self:GetPoint()}
    end)

    if not IFTSettings.show then
        trackerFrame:Hide()
    else
        -- Add the watcher.
        trackerFrame:SetScript("OnUpdate",onUpdate)
        trackerFrame:Show()
    end
end

-- Watcher for onLogin
local onLoginFrame = CreateFrame("Frame")
onLoginFrame:RegisterEvent("PLAYER_LOGIN")
onLoginFrame:SetScript("OnEvent",function(self, event)
    if event == "PLAYER_LOGIN" then
        -- Create the tracker frame.
        self:UnregisterEvent(event)

        -- Re check class in case it was not yet loaded.
        local class = strlower(select(2, UnitClass("player")))
        if class and class ~= "mage" then return end

        createTrackerFrame()
        SlashCmdList["IFT"]("help")
    end
end)

-- Add slash commands.
SlashCmdList["IFT"] = function(msg)
    local cmd, rest = msg:match("^(%S*)%s*(.-)$");

    if not InCombatLockdown() and cmd == "orientation" then
        IFTSettings.trackerOrientation = IFTSettings.trackerOrientation == "VERTICAL" and "HORIZONTAL" or "VERTICAL"
        setDimensions()
    elseif not InCombatLockdown() and (cmd == "width" or cmd == "height") and tonumber(rest) then
        rest = tonumber(rest)
        rest = (rest < 200 and rest > 5) and rest or 200

        if cmd == "width" then
            IFTSettings.trackerWidth = rest
        elseif cmd == "height"then
            IFTSettings.trackerHeight = rest
        end

        setDimensions()
    elseif not InCombatLockdown() and cmd == "show" then
        IFTSettings.show = not IFTSettings.show
        trackerFrame:SetScript("OnUpdate", IFTSettings.show and onUpdate or nil)

        if IFTSettings.show then
            trackerFrame:Show()
        else
            trackerFrame:Hide()
        end
    elseif cmd == "reset" then
        IFTSettings = nil
        IFTSettings = setmetatable({}, { __index = mtSVars });

        -- Reposition the frame.
        trackerFrame:ClearAllPoints()
        trackerFrame:SetPoint (unpack(IFTSettings.point))

        -- Reset the layout.
        setDimensions()
        onUpdate(trackerFrame)
        trackerFrame:SetScript("OnUpdate", IFTSettings.show and onUpdate or nil)
        trackerFrame:Show()
    else
        -- Addon Help menu
        print("--- Incanter's Flow Tracker ---")
        print("The following commands are available to use with \"/ift \".")
        print("\"show\"          - Toggle the Tracking Frame")
        print("\"orientation\" - Switch Layout Orientation")
        print("\"width #\"      - Change the Tracker Width  (5-200)")
        print("\"height #\"     - Change the Tracker Height (5-200)")
    end
end

SLASH_IFT1 = "/ift";